package com.Stepdefinitions;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/Feautres/Demowebshop.feature",
glue={"com.Stepdefinitions"},
plugin = { "html:target/Destination/report.html",
		"junit:target/Destination/report.xml",
		"json:target/Destination/report.json"})

public class JunitTestrunner {

}
