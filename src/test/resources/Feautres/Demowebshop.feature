Feature: Demo Web Shop

  @loginMethod
  Scenario: verify Login
    Given I am on the demo web shop login page
    When I enter the username and password
    And I click the login button
    Then I should be logged in successfully

  @booksMethod
  Scenario: verify Books
    Given I am on the demo web shop home page
    When I select the Books category
    And I sort the books by Price 'High to Low'
    And I add two books to the cart
    Then the two books should be added to the cart successfully

  @electronicsMethod
  Scenario: verify Electronics
    Given I am on the demo web shop home page
    When I select the Electronics category
    And I select a cell phone product
    And I add the product to the cart
    Then the count of items added to the cart should be displayed successfully

  @giftcardsMethod
  Scenario: verify Gift Cards
    Given I am on the demo web shop home page
    When I select the Gift Cards category
    And I display 4 items per page
    And I capture the name and price of one of the displayed gift cards
    Then the name and price should be captured successfully

  @logoutMethod
  Scenario: verify Logout
    Given I am logged in to the demo web shop
    When I click the logout button
    Then I should be logged out successfully
    And the login button should be displayed on the home page